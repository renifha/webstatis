@extends('layout.master')

@section('judul')
    <h1>Sign Up Form</h1>
@endsection

@section('content')
<h3>Buat Account Baru!</h3>

<form action="/welcome" method="POST">
    @csrf
    <label>First name: </label><br><br>
    <input type="text" name="firstname"><br><br>
    <label>Last name: </label><br><br>
    <input type="text" name="lastname"><br><br>
    <label>Gender: </label><br><br>
    <input type="radio" name="gender">Male<br>
    <input type="radio" name="gender">Female<br>
    <input type="radio" name="gender">Other<br><br>
    <label>Nationality: </label><br><br>
    <select name="national">
        <option value="IND">Indonesia</option>
        <option value="">Malaysia</option>
        <option value="SG">Singapura</option>
        <option value="KR">Korea</option>
        <option value="JPN">Japan</option>
    </select><br><br>
    <label>Language Spoken: </label><br><br>
    <input type="checkbox">Bahasa Indonesia<br>
    <input type="checkbox">English<br>
    <input type="checkbox">Other<br><br>
    <label>Bio: </label><br><br>
    <textarea name="bio"cols="30" rows="10"></textarea><br><br>
    <input type="submit" value="Sign Up">
</form>
@endsection
    
