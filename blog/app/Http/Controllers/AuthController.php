<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    public function register() {
        return view('others.register');
    }

    public function welcome(Request $request) {
        $firstName = $request['firstname'];
        $lastName = $request['lastname'];

        return view('others.welcome', compact('firstName','lastName'));
    }
}
